#!/bin/bash
##############################################################
# Pequeño script para lanzar un perfil de firefox 
# según el nombre del escritorio virtual en el que estemos
# hace falta tener instalado wmctrl
# Realizado por Manuel Canga ( dev@trasweb.net )
# Licencia: GPLv3
#############################################################
current_desktop=$(wmctrl -d|grep "*"|cut -d " " -f 14,14)
/usr/bin/firefox -no-remote -P $current_desktop 2>/dev/null
