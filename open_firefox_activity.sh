#!/bin/bash
##############################################################
# Pequeño script para lanzar un perfil de firefox
# según el nombre de la activity actual ( kde plasma )
# hace falta tener kde y plasma
# Realizado por Manuel Canga ( dev@trasweb.net )
# Licencia: GPLv3
#############################################################
current_activity=$(qdbus org.kde.kactivitymanagerd /ActivityManager/Activities CurrentActivity)
current_activity_name=$(qdbus org.kde.kactivitymanagerd /ActivityManager/Activities ActivityName $current_activity)
/usr/bin/firefox -no-remote -P $current_activity_name 2>/dev/null
