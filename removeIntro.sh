#!/bin/bash
#Removing first 31 seconds of each video in current directory
#Ref: http://askubuntu.com/questions/59383/extract-part-of-a-video-with-a-one-line-command
for VIDEO in *
do
ffmpeg -ss 00:00:31  -i $VIDEO -vcodec copy -acodec copy tmp/$VIDEO 
done
